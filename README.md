# Input-Attribute-Placeholder
Placeholder

<label for="demo">Placeholder demo</label> : <input id ="demo" placeholder="Support Placeholder" />

<script>
function testAttribute(element, attribute)
{
  var test = document.createElement(element);
  if (attribute in test) 
    return true;
  else 
    return false;
}

if (!testAttribute("input", "placeholder")) 
{
  window.onload = function() 
  {
    var demo = document.getElementById("demo");
    var text_content = "No Placeholder support";

    demo.style.color = "gray";
    demo.value = text_content;

    demo.onfocus = function() {
    if (this.style.color == "gray")
    { this.value = ""; this.style.color = "black" }
    }

    demo.onblur = function() {
    if (this.value == "")
    { this.style.color = "gray"; this.value = text_content; }
    }
  } 
}
</script> 
